var express = require("express"),
    mongoose = require('mongoose'),
    passport = require('passport'),
    bodyParser = require('body-parser'),
    localStrategy = require('passport-local'),
    passportLocalMongoose = require('passport-local-mongoose'),
    User = require('./models/users');

var app = express();
mongoose.connect("mongodb://localhost:27017/ide_lab");

app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine','ejs');
app.use(require('express-session')({
    secret:"this is the secret sentence",
    resave:false,
    saveUninitialized:false
}));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new localStrategy(User.authenticate()));


passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//Routes

app.get("/",function(req,res){
    res.render("home",{user:req.user});
})

app.get("/secret",isLoggedIn,function(req,res){
    console.log(req.user);
    res.render("secret");
})

app.get("/add",isLoggedIn,function(req,res){
    res.render('add');
});

app.get("/addsong",isLoggedIn,function(req,res){
    res.render('add_song');
});

app.post("/addsong",isLoggedIn,function(req,res){

    User.find(req.user,function(err,user){
        if(err)
            console.log(err);
        else{
            song = {};
            song.songId = req.body.songId;
            song.albumId = req.body.albumId;
            song.singer = req.body.singer;
            song.title = req.body.title;

            console.log(user);
            console.log(song);
            user[0].songs.push(song);
            user[0].save();
        }
    })
    res.redirect('/');
});

app.get("/addalbum",isLoggedIn,function(req,res){
    res.render('add_album');
});

app.post("/addalbum",isLoggedIn,function(req,res){

    User.find(req.user,function(err,user){
        if(err)
            console.log(err);
        else{
            album = {};
            album.title = req.body.title;
            album.albumId = req.body.albumId;
            album.copyright = req.body.copyright;
            album.format = req.body.format;

            console.log(user);
            console.log(album);
            user[0].albums.push(album);
            user[0].save();
        }
    })
    res.redirect('/');
});

app.get('/songs',isLoggedIn,function(req,res){
    User.find(req.user,function(err,user){
        if(err)
            console.log(err);
        else{
            res.render('songs',{songs:user[0].songs});
        }
    })
});


app.get('/albums',isLoggedIn,function(req,res){
    User.find(req.user,function(err,user){
        if(err)
            console.log(err);
        else{
            res.render('albums',{albums:user[0].albums});
        }
    })
});
//==========Auth routes================

app.get("/register",function(req,res){
    res.render("register");
})

app.post('/register',function(req,res){
    User.register(new User({username: req.body.username}),req.body.password,function(err, user){
        if(err){
            console.log(err);
            return res.render('/register');
        }
        passport.authenticate("local")(req,res, function(){
            res.redirect('/secret');
        })
    })
})

app.get('/login',function(req,res){
    res.render('login');
});

app.post('/login',passport.authenticate('local',{
    successRedirect:'/',
    failureRedirect:'/login'
}),function(req,res){
    alert('Logged in');
});

function isLoggedIn(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }else{
        res.redirect("/login");
    }
}


app.get("/logout",function(req,res){
    req.logout();
    res.redirect("/");
});


var port = 3000;
app.listen(port,process.env.IP,function(){
    console.log("server started...");
})